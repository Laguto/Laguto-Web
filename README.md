# Laguto-Web

Angular application to visualize data from [Laguto](https://codeberg.org/laguto/Laguto-Interface) in the browser.

[Website](https://laguto.de) - [Documentation](https://docs.laguto.de)

# Authors

- [Felix Grohme](https://social.lauercloud.de/@felix)
- [Mike Lauer](https://social.lauercloud.de/@mike)