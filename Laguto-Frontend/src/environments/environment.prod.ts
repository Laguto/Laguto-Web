export const environment = {
  production: true,
  localToolURL: 'http://localhost:5000/api/',
  localToolPort: "5000",
  storageSettingsPath: "laguto-settings",
  version: "1.1"
};
