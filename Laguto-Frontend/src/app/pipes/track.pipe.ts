import { Pipe, PipeTransform } from '@angular/core';
import {Track} from "../models/enums/track";

@Pipe({
  name: 'track'
})
export class TrackPipe implements PipeTransform {

  transform(value:number): string {
    return Track[value];
  }

}
