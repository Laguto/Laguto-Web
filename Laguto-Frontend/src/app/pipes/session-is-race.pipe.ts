import {Pipe, PipeTransform} from '@angular/core';
import {SessionType} from "../models/enums/sessionType";

@Pipe({
  name: 'sessionIsRace'
})
export class SessionIsRacePipe implements PipeTransform {

  transform(session: SessionType): boolean {
    switch(session) {
      case SessionType.Race:
      case SessionType.Race2:
      case SessionType.Race3:
        return true;
      default:
        return false
    }
  }

}
