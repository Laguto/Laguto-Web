import { Pipe, PipeTransform } from '@angular/core';
import {formatDate} from "@angular/common";

@Pipe({
  name: 'deltaTime'
})
export class DeltaTimePipe implements PipeTransform {

  transform(time: number): string {
    if(time == 0)
      return '';

    let negative: boolean = time < 0
    if(negative)
      time *= -1;

    let formattedTime: string = ""
    if(time > 60000)
      formattedTime = formatDate(new Date(time), "m:ss.SSS", "de-DE");
    else
      formattedTime = formatDate(new Date(time), "s.SSS", "de-DE");

    // Rounding errors
    if(formattedTime === '0.001')
      return ""

    if(negative)
      formattedTime = "-" + formattedTime
    else
      formattedTime = "+" + formattedTime

    return formattedTime;
  }

}
