import { Pipe, PipeTransform } from '@angular/core';
import {SessionType} from "../models/enums/sessionType";

@Pipe({
  name: 'sessionType'
})
export class SessionTypePipe implements PipeTransform {

  transform(value: number): string {
    return SessionType[value];
  }

}
