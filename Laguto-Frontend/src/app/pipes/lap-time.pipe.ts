import { Pipe, PipeTransform } from '@angular/core';
import {formatDate} from "@angular/common";

@Pipe({
  name: 'lapTime'
})
export class LapTimePipe implements PipeTransform {

  transform(time: number, format: "Minutes"|"Seconds" = "Minutes"): string {
    if(time === 0)
      return '';

    let negative: boolean = time < 0;
    if(negative)
      time *= -1;

    let formattedTime: string = ""
    switch(format) {
      case "Seconds":
        formattedTime = formatDate(new Date(time), "s.SSS", "de-DE");
        break;
      case "Minutes":
      default:
        formattedTime = formatDate(new Date(time), "m:ss.SSS", "de-DE");
        break;
    }

    return formattedTime;
  }

}
