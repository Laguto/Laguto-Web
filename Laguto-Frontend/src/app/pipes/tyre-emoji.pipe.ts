import { Pipe, PipeTransform } from '@angular/core';
import {TyreEmoji} from "../models/enums/tyreEmoji";

@Pipe({
  name: 'tyreEmoji'
})
export class TyreEmojiPipe implements PipeTransform {

  transform(value: number): string {
    return TyreEmoji[value];
  }

}
