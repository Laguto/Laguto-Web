import { Pipe, PipeTransform } from '@angular/core';
import {WeatherCondition} from "../models/enums/weatherCondition";

@Pipe({
  name: 'weatherIcon'
})
export class WeatherIconPipe implements PipeTransform {

  transform(value: number): string {
    switch (value) {
      case WeatherCondition.Clear:
        return "☀";
      case WeatherCondition.LightClouds:
        return "🌤";
      case WeatherCondition.Overcast:
        return "☁";
      case WeatherCondition.LightRain:
        return "🌧";
      case WeatherCondition.HeavyRain:
        return "⛈";
      case WeatherCondition.Storm:
        return "🌪";
      default:
        return "☃"
    }
  }
}
