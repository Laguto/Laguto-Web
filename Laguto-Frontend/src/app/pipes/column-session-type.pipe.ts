import {Pipe, PipeTransform} from '@angular/core';
import {SessionType} from "../models/enums/sessionType";
import {ColumnSessionType} from "../models/enums/ColumnSessionType";

@Pipe({
  name: 'columnSessionType'
})
export class ColumnSessionTypePipe implements PipeTransform {

  transform(type : SessionType): ColumnSessionType {
    switch (type){
      case SessionType.Race:
      case SessionType.Race2:
      case SessionType.Race3:
        return ColumnSessionType.Race;

      default:
        return ColumnSessionType.NonRace;
    }

    return ColumnSessionType.All
  }
}
