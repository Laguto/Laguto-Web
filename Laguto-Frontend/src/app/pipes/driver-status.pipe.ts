import { Pipe, PipeTransform } from '@angular/core';
import {DriverResultStatus} from "../models/enums/driverResultStatus";

@Pipe({
  name: 'driverStatus'
})
export class DriverStatusPipe implements PipeTransform {

  transform(value: number): string {
    return DriverResultStatus[value];
  }

}
