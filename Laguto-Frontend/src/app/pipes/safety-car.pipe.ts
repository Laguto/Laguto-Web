import { Pipe, PipeTransform } from '@angular/core';
import {SafetyCarStatus} from "../models/enums/safetyCarStatus";

@Pipe({
  name: 'safetyCar'
})
export class SafetyCarPipe implements PipeTransform {

  transform(value: number): string {
    return SafetyCarStatus[value];
  }
}
