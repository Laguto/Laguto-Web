import { Injectable } from '@angular/core';
import {AbstractService} from "./abstract.service";
import {ConnectionService} from "./connection.service";
import {ConfigurationService} from "./configuration.service";
import {environment} from "../../environments/environment";
import {ResultComponent} from "../dashboard/result/result.component";

@Injectable({
  providedIn: 'root'
})
export class ExportService extends AbstractService {

  override eventId = "IsRaceFinished"

  resultComponent?: ResultComponent

  askedForDownload: boolean = false; // Only ask once to download

  constructor(private connectionService: ConnectionService,
              private configurationService: ConfigurationService) {
    super()
    this.connectionService.registerService(this);

  }

  override update(next: any) {
    let data = next as boolean;

    if (data
      && this.configurationService.Settings.AskForResultExport
      && !this.askedForDownload) {
      this.resultComponent?.showPopup()
      this.askedForDownload = true
    }
    if(!data)
      this.askedForDownload = false;
  }
}
