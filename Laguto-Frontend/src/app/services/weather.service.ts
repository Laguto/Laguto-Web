import {Injectable} from '@angular/core';
import {WeatherModel} from "../models/weather";
import {ConnectionService} from "./connection.service";
import {AbstractService} from "./abstract.service";
import {environment} from "../../environments/environment";
import {WeatherDTO} from "../models/DTOs/weatherDTO";

@Injectable({
  providedIn: 'root'
})
export class WeatherService extends AbstractService {

  override eventId = "Weather"

  weatherForecast: Array<WeatherModel>

  constructor(private connectionService: ConnectionService) {
    super()
    this.connectionService.registerService(this);

    this.weatherForecast = new Array<WeatherModel>()
  }

  override update(next: any) {
    let data = (next as WeatherDTO).WeatherModels;
    this.weatherForecast = data.sort((a, b) => a.SessionType - b.SessionType)
  }
}
