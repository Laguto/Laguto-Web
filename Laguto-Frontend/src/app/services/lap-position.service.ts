import { Injectable } from '@angular/core';
import {AbstractService} from "./abstract.service";
import {ConnectionService} from "./connection.service";
import {LapPositionComponent} from "../dashboard/lap-position/lap-position.component";
import {LapPosition} from "../models/lapPosition";

@Injectable({
  providedIn: 'root'
})
export class LapPositionService extends AbstractService {

  lapPositionComponent?: LapPositionComponent

  constructor(private connectionService: ConnectionService) {
    super();
    this.connectionService.registerService(this)
  }

  eventId: string = "LapPosition";

  update(next: any): void {
    let data = next as LapPosition

    if(this.lapPositionComponent) {

      // Compatibility for older Laguto Interface version
      if(data.TrackPositions.length == 0)
        return

      this.lapPositionComponent.clearCanvas()

      // Marshal zones
      for(let i = 0; i < data.NumberOfMarshallZones; i++) {
        let marshalZone = data.MarshallZones[i]
        if(i == 0)
          marshalZone.ZoneStart = 0

        if(i < data.NumberOfMarshallZones - 1)
          this.lapPositionComponent.drawMarshalZone(marshalZone.ZoneStart, data.MarshallZones[i+1].ZoneStart, marshalZone.ZoneFlag);
        else
          this.lapPositionComponent.drawMarshalZone(marshalZone.ZoneStart, 1, marshalZone.ZoneFlag);
      }

      // Line
      this.lapPositionComponent.drawLine()

      // Drivers

      let playerPosition = -1
      if(data.PlayerIndex >= 0 && data.PlayerIndex < data.TrackPositions.length)
        playerPosition = data.TrackPositions[data.PlayerIndex].Position

      data.TrackPositions.sort((a,b) => b.Position - a.Position)

      for(let i = 0; i < data.TrackPositions.length; i++) {
        let trackPosition = data.TrackPositions[i]
        if(!trackPosition || trackPosition.Position == 0) continue;

        let position = trackPosition.LapDistance / data.TrackLength
        let isPlayer = trackPosition.Position == playerPosition
        if(isPlayer)
          this.lapPositionComponent.drawPlayer(position, trackPosition.Color, trackPosition.IsAiControlled)
        else
          this.lapPositionComponent.drawDriver(position, trackPosition.Color, trackPosition.Position, trackPosition.IsAiControlled)
      }
    }
  }
}
