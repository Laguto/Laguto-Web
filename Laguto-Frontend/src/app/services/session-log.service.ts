import {Injectable} from '@angular/core';
import {AbstractService} from "./abstract.service";
import {ConnectionService} from "./connection.service";
import {SessionLogDTO} from "../models/DTOs/sessionLogDTO";
import {SessionEventType} from "../models/sessionEvents/eventType";
import {SessionEvent} from "../models/sessionEvents/sessionEvent";
import {
  SessionEventDataDriveThroughPenaltyServed,
  SessionEventDataFastestLap,
  SessionEventDataPenalty, SessionEventDataRaceWinner,
  SessionEventDataRetirement, SessionEventDataStopGoPenaltyServed
} from "../models/sessionEvents/eventData";
import {SessionLogEntry} from "../models/sessionLogEntry";
import {TableService} from "./table.service";
import {LapTimePipe} from "../pipes/lap-time.pipe";
import {PenaltyType} from "../models/enums/penaltyType";
import {InfringementType} from "../models/enums/infringementType";
import {MessageService} from "primeng/api";

@Injectable({
  providedIn: 'root'
})
export class SessionLogService extends AbstractService {

  eventId = "SessionLog";

  data: Array<SessionLogEntry>

  shownToastEntries: Array<string>

  constructor(private connectionService : ConnectionService, private tableService: TableService, private messageService: MessageService) {
    super();
    this.connectionService.registerService(this)

    this.data = new Array<SessionLogEntry>()
    this.shownToastEntries = new Array<string>()
  }

  update(next: any): void {
    let data = next as SessionLogDTO


    if(!data.SessionLog || data.SessionLog.length == 0)
      return

    let newData = new Array<SessionLogEntry>()

    let entry: any
    data.SessionLog.forEach(event => {
      switch (event.EventType) {
        case SessionEventType.LightsOut:
          entry = this.getEntryFromLightsOut()
          newData.push(entry)
          break
        case SessionEventType.FastestLap:
          entry = this.getEntryFromFastestLap(event)
          if(this.shownToastEntries.indexOf(JSON.stringify(entry)) == -1) {
            this.messageService.add({key: 'session-log', severity:'purple', summary: entry.title, detail: entry.details, life: 5000});
            this.shownToastEntries.push(JSON.stringify(entry))
          }
          newData.push(entry)
          break
        case SessionEventType.Retirement:
          entry = this.getEntryFromRetirement(event)
          if(this.shownToastEntries.indexOf(JSON.stringify(entry)) == -1) {
            this.messageService.add({key: 'session-log', severity:'grey', summary: entry.title, detail: entry.details, life: 5000});
            this.shownToastEntries.push(JSON.stringify(entry))
          }
          newData.push(entry)
          break
        case SessionEventType.RcDRSEnabled:
          entry = this.getEntryFromRcDRSEnabled()
          if(this.shownToastEntries.indexOf(JSON.stringify(entry)) == -1) {
            this.messageService.add({key: 'session-log', severity:'success', summary: entry.title, detail: entry.details, life: 5000});
            this.shownToastEntries.push(JSON.stringify(entry))
          }
          newData.push(entry)
          break
        case SessionEventType.RcDRSDisabled:
          entry = this.getEntryFromRcDRSDisabled()
          newData.push(entry)
          break
        case SessionEventType.CheckeredFlagWaved:
          this.getEntryFromCheckeredFlagWaved()
          newData.push(entry)
          break
        case SessionEventType.RaceWinner:
          entry = this.getEntryFromRaceWinner(event)
          newData.push(entry)
          break
        case SessionEventType.Penalty:
          entry = this.getEntryFromPenalty(event)
          if(this.shownToastEntries.indexOf(JSON.stringify(entry)) == -1) {
            this.messageService.add({key: 'session-log', severity:'warn', summary: entry.title, detail: entry.details, life: 5000});
            this.shownToastEntries.push(JSON.stringify(entry))
          }
          newData.push(entry)
          break
        case SessionEventType.DriveThroughPenaltyServed:
          entry = this.getEntryFromDriveThroughPenaltyServed(event)
          newData.push(entry)
          break
        case SessionEventType.StopGoPenaltyServed:
          entry = this.getEntryFromStopGoPenaltyServed(event)
          newData.push(entry)
          break
      }
    })

    this.data = newData.reverse()
  }

  getEntryFromLightsOut() {
    return this.createSessionLogEntry("Lights Out","")
  }

  getEntryFromFastestLap(event: SessionEvent) {
    let details = event.EventDetails as SessionEventDataFastestLap
    let message = this.tableService.getDriverNameFromIndex(details.CarIndex) + " " + (new LapTimePipe().transform(details.LapTime))
    return this.createSessionLogEntry("Fastest Lap",message)
  }

  getEntryFromRetirement(event: SessionEvent) {
    let details = event.EventDetails as SessionEventDataRetirement
    let message = this.tableService.getDriverNameFromIndex(details.CarIndex)
    return this.createSessionLogEntry("Car Retired",message)
  }

  getEntryFromRcDRSEnabled() {
    return this.createSessionLogEntry("Race Control","DRS Enabled")
  }

  getEntryFromRcDRSDisabled() {
    return this.createSessionLogEntry("Race Control","DRS Disabled")
  }

  getEntryFromCheckeredFlagWaved() {
    return this.createSessionLogEntry("Checkered Flag Waved", "")
  }

  getEntryFromRaceWinner(event: SessionEvent) {
    let details = event.EventDetails as SessionEventDataRaceWinner
    let message = this.tableService.getDriverNameFromIndex(details.CarIndex)
    return this.createSessionLogEntry("Race Winner", message)
  }

  getEntryFromPenalty(event: SessionEvent) {
    let details = event.EventDetails as SessionEventDataPenalty

    let message = this.tableService.getDriverNameFromIndex(details.CarIndex) + " " +
      (details.Time == 255 ? "" : details.Time + "s") + " " +
      this.camelCaseToText(InfringementType[details.InfringementType])
    return this.createSessionLogEntry(this.camelCaseToText(PenaltyType[details.PenaltyType]), message)
  }

  getEntryFromDriveThroughPenaltyServed(event: SessionEvent) {
    let details = event.EventDetails as SessionEventDataDriveThroughPenaltyServed
    let message = this.tableService.getDriverNameFromIndex(details.CarIndex)
    return this.createSessionLogEntry("Drive through penalty served", message)
  }

 getEntryFromStopGoPenaltyServed(event: SessionEvent) {
   let details = event.EventDetails as SessionEventDataStopGoPenaltyServed
   let message = this.tableService.getDriverNameFromIndex(details.CarIndex)
   return this.createSessionLogEntry("Stop and go penalty served", message)
 }


  createSessionLogEntry(s: string, s2: string) {
    return new class implements SessionLogEntry {
      title = s;
      details = s2;
    }
  }

  camelCaseToText(input: string) {
    return input.replace(/([A-Z])/g, " $1")
  }

  getData(): Array<SessionLogEntry> {
    return this.data
  }
}
