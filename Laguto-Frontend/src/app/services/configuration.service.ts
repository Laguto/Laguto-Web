import { Injectable } from '@angular/core';
import {MessageService} from "primeng/api";
import {HttpClient} from "@angular/common/http";
import {environment} from "../../environments/environment";
import {ColumnSessionType} from "../models/enums/ColumnSessionType";
import {Settings} from "../models/settings";

@Injectable({
  providedIn: 'root'
})
export class ConfigurationService {

  Settings: Settings;

  selectedTableColumnsIds: Array<string>

  constructor(private http: HttpClient, private messageService: MessageService) {
    let version = this.loadVersion();

    this.Settings = this.getDefaultSettings();
    let loadDefault = true

    if(version != null && version == environment.version){
      let settings = this.loadSettings();

      if(settings != null) {
        this.Settings = settings
        loadDefault = false
      }
    }

    if(loadDefault) {
      localStorage.setItem("laguto-version", environment.version);
      this.saveSettings();
    }

    this.selectedTableColumnsIds = this.Settings.TableSettings.Columns.filter(x => x.enabled).map(x => x.id)
  }

  updateSelectedTableColumns() {
    this.Settings.TableSettings.Columns
      .forEach(x => {
        x.enabled = this.selectedTableColumnsIds.includes(x.id);
      });
    this.saveSettings();
  }

  set HighlightPlayerEnabled(val: boolean){
    this.Settings.TableSettings.HighlightPlayer = val;
    this.saveSettings();
  }

  get HighlightPlayerEnabled() {
    return this.Settings.TableSettings.HighlightPlayer;
  }

  set AskForResultExportEnabled(val: boolean){
    this.Settings.AskForResultExport = val;
    this.saveSettings();
  }

  get AskForResultExportEnabled() {
    return this.Settings.AskForResultExport;
  }

  set ModuleTableEnabled(val: boolean){
    this.Settings.ViewSettings.Table = val;
    this.saveSettings();
  }

  get ModuleTableEnabled() {
    return this.Settings.ViewSettings.Table;
  }

  set ModuleLapPositionEnabled(val: boolean){
    this.Settings.ViewSettings.LapPosition = val;
    this.saveSettings();
  }

  get ModuleLapPositionEnabled() {
    return this.Settings.ViewSettings.LapPosition;
  }

  set ModuleSessionInfoEnabled(val: boolean){
    this.Settings.ViewSettings.SessionInfo = val;
    this.saveSettings();
  }

  get ModuleSessionInfoEnabled() {
    return this.Settings.ViewSettings.SessionInfo;
  }

  set ModuleLapTimeEnabled(val: boolean){
    this.Settings.ViewSettings.LapTime = val;
    this.saveSettings();
  }

  get ModuleLapTimeEnabled() {
    return this.Settings.ViewSettings.LapTime;
  }

  set ModuleCarDamageEnabled(val: boolean){
    this.Settings.ViewSettings.CarDamage = val;
    this.saveSettings();
  }

  get ModuleCarDamageEnabled() {
    return this.Settings.ViewSettings.CarDamage;
  }

  set ModuleWeatherEnabled(val: boolean){
    this.Settings.ViewSettings.Weather = val;
    this.saveSettings();
  }

  get ModuleWeatherEnabled() {
    return this.Settings.ViewSettings.Weather;
  }

  set ModuleSessionLogEnabled(val: boolean){
    this.Settings.ViewSettings.SessionLog = val;
    this.saveSettings();
  }

  get ModuleSessionLogEnabled() {
    return this.Settings.ViewSettings.SessionLog;
  }

  set CompactTableNumberCars(val: number){
    this.Settings.TableSettings.CompactTableSettings.NumberOfCars = val;
    this.saveSettings();
  }

  get CompactTableNumberCars() {
    return this.Settings.TableSettings.CompactTableSettings.NumberOfCars;
  }

  set CompactTableSettingsEnabled(val: boolean){
    this.Settings.TableSettings.CompactTableSettings.Enabled = val;
    this.saveSettings();
  }

  get CompactTableSettingsEnabled() : boolean {
    return this.Settings.TableSettings.CompactTableSettings.Enabled;
  }

  private loadSettings(): Settings | null {
    let cookieString = localStorage.getItem(environment.storageSettingsPath);

    if(cookieString == null)
      return null

    return JSON.parse(cookieString);
  }

  private getDefaultSettings() : Settings {
    return {
      test: false,
      AskForResultExport: false,
      ViewSettings: {
        Table: true,
        SessionInfo: true,
        TrackStatus: true,
        LapTime: true,
        CarDamage: true,
        Weather: true,
        LapPosition: true,
        SessionLog: false
      },
      TableSettings: {
        HighlightPlayer: true,
        CompactTableSettings: {
          Enabled: false,
          NumberOfCars: 11,
        },
        Columns: [
          {id: "team", header:"T",label:"Team", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "position",header:"P",label:"Position", isChangeable: true, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "driver",header:"Driver",label:"Driver", isChangeable: true, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "best_lap",header:"Best Lap",label:"Best Lap", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "delta_best",header:"Delta Best",label:"Delta Best", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "delta_front",header:"Delta Front",label:"Delta Front", isChangeable: false, allowedSessionTypes: ColumnSessionType.NonRace, enabled: true},
          {id: "last_lap",header:"Last Lap",label:"Last Lap", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "delta_front_last",header:"Delta Front",label:"Delta Front Last", isChangeable: false, allowedSessionTypes: ColumnSessionType.Race, enabled: true},
          {id: "interval",header:"Interval",label:"Interval", isChangeable: false, allowedSessionTypes: ColumnSessionType.Race, enabled: true},
          {id: "leader",header:"Leader",label:"Leader", isChangeable: false, allowedSessionTypes: ColumnSessionType.Race, enabled: true},
          {id: "status",header:"Status",label:"Status", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "pit",header:"Pit",label:"Pit", isChangeable: false, allowedSessionTypes: ColumnSessionType.Race, enabled: true},
          {id: "s1_best",header:"S1 Best",label:"S1 Best", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "s2_best",header:"S2 Best",label:"S2 Best", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "s3_best",header:"S3 Best",label:"S3 Best", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "s1_last",header:"S1 Last",label:"S1 Last", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "s2_last",header:"S2 Last",label:"S2 Last", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "s3_last",header:"S3 Last",label:"S3 Last", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "tyre",header:"Tyre (Age)",label:"Tyre", isChangeable: false, allowedSessionTypes: ColumnSessionType.All, enabled: true},
          {id: "position_change",header:"+/-",label:"+/-", isChangeable: false, allowedSessionTypes: ColumnSessionType.Race, enabled: true},
          {id: "penalties",header:"Penalties",label:"Penalties", isChangeable: false, allowedSessionTypes: ColumnSessionType.Race, enabled: true}
        ]
      }
    }
  }

  public saveSettings(){
    localStorage.setItem(environment.storageSettingsPath, JSON.stringify(this.Settings));
  }

  private loadVersion(): string | null {
    return localStorage.getItem("laguto-version");
  }
}
