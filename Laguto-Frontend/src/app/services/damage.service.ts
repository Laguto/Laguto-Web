import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {LapTimesDTO} from "../models/DTOs/lapTimesDTO";
import {environment} from "../../environments/environment";
import {DamageDTO} from "../models/DTOs/damageDTO";
import {ConnectionService} from "./connection.service";
import {AbstractService} from "./abstract.service";
import {CarDamage} from "../models/carDamage";
import {SessionInfo} from "../models/sessionInfo";

@Injectable({
  providedIn: 'root'
})
export class DamageService extends AbstractService {

  override eventId = "CarDamage"

  damage: CarDamage
  size: number = 80

  constructor(private connectionService: ConnectionService) {
    super()
    this.connectionService.registerService(this);

    this.damage = {
      FrontLeftWingDamage: 0,
      FrontRightWingDamage: 0,
      RearWingDamage: 0,
      FloorDamage: 0,
      DiffuserDamage: 0,
      SidepodDamage: 0,
    }
  }

  override update(next: any) {
    let data = next as DamageDTO
    this.damage = data.DamageModel
  }

}
