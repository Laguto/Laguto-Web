import { Injectable } from '@angular/core';
import {Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export abstract class AbstractService {

  abstract eventId: string // ID that MUST match the type coming from the local tool

  /**
   * Gets specific DTO object as a generic object
   * First thing to do in the implementation is
   * casting to the desired DTO format
   */
  abstract update(next: any): void
}
