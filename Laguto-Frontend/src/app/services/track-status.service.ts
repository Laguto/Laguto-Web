import {Injectable} from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {TableDTO} from "../models/DTOs/tableDTO";
import {environment} from "../../environments/environment";
import {MarshallZone, TrackStatus} from "../models/trackStatus";
import {AbstractService} from "./abstract.service";
import {ConnectionService} from "./connection.service";
import {FiaFlag} from "../models/enums/fiaFlag";
import {SafetyCarStatus} from "../models/enums/safetyCarStatus";

@Injectable({
  providedIn: 'root'
})
export class TrackStatusService extends AbstractService {

  override eventId = "MarshallZones"

  trackStatus: TrackStatus
  marshallZonesSeverity: Array<string>

  constructor(private connectionService: ConnectionService) {
    super()
    this.connectionService.registerService(this);

    this.trackStatus = {
      NumberOfMarshallZones: 0,
      MarshallZones: new Array<MarshallZone>(),
      SafetyCarStatus: SafetyCarStatus.None
    }

    this.marshallZonesSeverity = new Array<string>()
  }

  override update(next: any) {
    let data = next as TrackStatus;
    this.trackStatus = data

    if (data.MarshallZones == null)
      return

    this.marshallZonesSeverity = data.MarshallZones.filter(x => x.ZoneStart > 0).map(x => {
      switch (x.ZoneFlag) {
        case FiaFlag.Yellow:
          return "warning"
        case FiaFlag.Red:
          return "danger"
        default:
          return "success"
      }
    })
  }

}
