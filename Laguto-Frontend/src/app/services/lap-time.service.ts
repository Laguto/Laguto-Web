import { Injectable } from '@angular/core';
import {Observable} from "rxjs";
import {TableDTO} from "../models/DTOs/tableDTO";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {LapTimesDTO} from "../models/DTOs/lapTimesDTO";
import {AbstractService} from "./abstract.service";
import {ConnectionService} from "./connection.service";
import {LapTimeModel} from "../models/lapTime";
import {TableService} from "./table.service";

@Injectable({
  providedIn: 'root'
})
export class LapTimeService extends  AbstractService{

  override eventId = "LapTime"

  basicData : any;
  playerPosition : number;

  constructor(private connectionService : ConnectionService, private tableService: TableService) {
    super()
    this.connectionService.registerService(this);

    this.basicData = {
      labels: [],
      datasets: []
    };
    this.playerPosition = 0;
  }

  override update(next: any) {
    let data = next as LapTimesDTO

    if(data.LapTimes.length == 0)
      return

    this.playerPosition = data.PlayerPosition;

    if(this.playerPosition == 0)
      return

    let newData = {
      labels: new Array<number>(),
      datasets: new Array<any>()
    };
    data.LapTimes[this.playerPosition - 1].LapTimes.forEach((value, index) => {
      newData.labels.push(index+1)
    });
    newData.datasets = data
      .LapTimes
      .filter(x => Math.abs(this.playerPosition - x.Position) <= 1)
      .map(x => this.getObjectFromLapTimes(x, x.Position == this.playerPosition, x.Position - this.playerPosition > 0));

    if(JSON.stringify(this.basicData) != JSON.stringify(newData)) {
      this.basicData = newData
    }
  }

  getObjectFromLapTimes(x: LapTimeModel, player: boolean, driverBehind: boolean) {
    let name = this.tableService.getDriverNameFromPosition(x.Position);

    let obj = {
      label: name,
      data: x.LapTimes,
      fill: false,
      backgroundColor : "rgba(0,0,0,0)",
      borderColor: "rgb(5,212,248)",
      borderDash: Array<Number>(),
      tension: .4
    };

    if (player) {
      obj.fill = true;
      obj.backgroundColor = "rgba(44, 185, 0, 0.1)"
      obj.borderColor = "rgb(44, 185, 0)"
    } else if (driverBehind) {
      obj.borderColor = "rgb(243,72,6)"
      obj.borderDash = [5, 5]
    }

    return obj;
  }

  }
