import {Injectable, NgZone} from '@angular/core';
import {MessageService} from "primeng/api";
import {AbstractService} from "./abstract.service";
import {environment} from "../../environments/environment";
import {HttpClient} from "@angular/common/http";
import {Observable, Subscription} from "rxjs";

@Injectable({
  providedIn: 'root'
})
export class ConnectionService {

  private services: Array<AbstractService> // Services that want updates
  private subscriptions: Array<Subscription>
  private eventSources: Array<EventSource>
  private connectionsCounter: number = 0

  testingConnection: boolean = false
  connectionFailed: boolean = false

  private numberOfStreams = 3

  newVersionAvailable = false

  constructor(private _zone: NgZone, private messageService: MessageService, private http: HttpClient) {
    this.subscriptions =  new Array<Subscription>()
    this.eventSources = new Array<EventSource>()
    this.services = new Array<AbstractService>()
  }

  /**
   * Public interface to start listening
   */
  start() {
    this.testConnection()
  }

  isConnected() : boolean {
    return this.connectionsCounter == this.numberOfStreams
  }

  /**
   * Stop all listeners and initiate ping test
   */
  private testConnection() {
    if(this.testingConnection)
      return

    this.testingConnection = true
    this.connectionFailed = false
    this.connectionsCounter = 0
    this.unsubscribeAllServices()
    this.pingLocalTool()
  }

  /**
   * Stop all current connections and subscriptions
   */
  private unsubscribeAllServices() {
    this.subscriptions.forEach(sub => {
      if(sub?.closed == false)
        sub.unsubscribe()
    })
    this.eventSources.forEach(x => x.close())
    this.subscriptions = new Array<Subscription>()
    this.eventSources = new Array<EventSource>()
  }

  /**
   * Ping local backend.
   * If successful connect streams
   * Otherwise set error flag
   */
  private pingLocalTool() {
    const url = environment.localToolURL + "ping"
    this.http.get(url).subscribe({
      next: (data) => {
        if(typeof data === "boolean")
          this.newVersionAvailable = data as boolean
        if(!this.isConnected()) {
          this.connectAll()
        }
      },
      error: (error) => {
        this.messageService.add({severity:'error', summary:'Connection failed', detail:'No connection possible', life: 5000});
        this.connectionFailed = true
        this.testingConnection = false
      }
      })
  }

  /**
   * Connect all streams
   */
  connectAll() {
    let fastObs = this.connect("fast")
    let mediumObs = this.connect("medium")
    let longObs = this.connect("long")

    let handler = (next: string) => {
      let data = JSON.parse(next)
      this.services.find(x => x.eventId == data.type)?.update(data.data)
    }

    let fastSub = fastObs.subscribe(handler)
    let mediumSub = mediumObs.subscribe(handler)
    let longSub = longObs.subscribe(handler)

    this.subscriptions.push(fastSub)
    this.subscriptions.push(mediumSub)
    this.subscriptions.push(longSub)
  }

  /**
   * Connect to stream
   */
  connect(speed: string): Observable<string> {
    const url = environment.localToolURL+ "update/" + speed;
    return new Observable<string>(observer => {
      const eventSource = new EventSource(url)
      this.eventSources.push(eventSource)

      eventSource.onmessage = event => {
        this._zone.run(() => {
          observer.next(event.data);
        });
      };

      eventSource.onopen = event => {
        this._zone.run(() => {
          /**
           * If this stream is the last to be connected
           * show success message
           */
          if(++this.connectionsCounter == this.numberOfStreams) {
            this.messageService.add({severity:'success', summary:'Laguto connected', detail:'Connection established', life: 5000});
            this.testingConnection = false
          }
        });
      };

      eventSource.onerror = error => {
        this._zone.run(() => {
          /**
           * One stream experienced an error
           * If it is the first one to fail, show error message
           */
          if(!this.connectionFailed) {
            this.connectionFailed = true
            this.messageService.add({severity:'error', summary:'Laguto disconnected', detail:'Connection failed', life: 5000});
          }

          this.stop() // stop all services
        });
      };
    })
  }

  /**
   * Interface for services for registration
   */
  registerService(service: AbstractService) {
    this.services.push(service)
  }

  /**
   * Stops all connections
   */
  stop() {
    this.unsubscribeAllServices()
    this.connectionsCounter = 0
    this.testingConnection = false
  }
}
