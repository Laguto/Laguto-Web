import {Injectable} from '@angular/core';
import {ConnectionService} from "./connection.service";
import {AbstractService} from "./abstract.service";
import {TableRowModel} from "../models/tableRow";
import {SessionType} from "../models/enums/sessionType";
import {TableDTO} from "../models/DTOs/tableDTO";
import {ConfigurationService} from "./configuration.service";
import {ColumnSessionTypePipe} from "../pipes/column-session-type.pipe";
import {ColumnSessionType} from "../models/enums/ColumnSessionType";

@Injectable({
  providedIn: 'root'
})
export class TableService extends AbstractService {

  override eventId = "Table"

  tableRowModels: Array<TableRowModel>

  playerIndex: number

  bestLapTimePlayerIndex: number = -1;
  bestSector1PlayerIndex: number = -1;
  bestSector2PlayerIndex: number = -1;
  bestSector3PlayerIndex: number = -1;

  sessionType: SessionType
  columnSessionType: ColumnSessionType

  constructor(private connectionService: ConnectionService, public configurationService : ConfigurationService) {
    super()
    this.connectionService.registerService(this)

    this.tableRowModels = new Array<TableRowModel>()
    this.playerIndex = -1

    this.sessionType = SessionType.Unknown
    this.columnSessionType = ColumnSessionType.Unknown
  }

  override update(next: any) {
    let data = next as TableDTO;

    if(data.SessionType != this.sessionType){
      this.sessionType = data.SessionType
      this.columnSessionType = new ColumnSessionTypePipe().transform(this.sessionType)
    }

    let rows  = data.RowModels?.filter(x => x.Position > 0)
      .sort((a,b) => this.sortRowsByPosition(a,b))

    this.computeBestTimesIndexes(rows)

    this.playerIndex = data.PlayerIndex;

    /* Compute compact mode */
    if(this.configurationService.CompactTableSettingsEnabled && rows.length > 0) {
      let playerPosition = rows.filter(row => row.PlayerIndex == this.playerIndex)[0].Position

      let numberOfOtherCars = this.configurationService.CompactTableNumberCars - 1
      let numberOfCarsInFront = Math.round(numberOfOtherCars / 2)
      let numberOfCarsBehind = numberOfOtherCars - numberOfCarsInFront

      let startIndex = playerPosition - numberOfCarsInFront - 1
      let endIndex = playerPosition + numberOfCarsBehind - 1

      if(startIndex < 0) {
        endIndex += startIndex * (-1)
        startIndex = 0
      }

      // If PositionToShow > NumberOfCars
      if(endIndex > rows.length - 1) {
        startIndex -= (endIndex + 1) - rows.length // difference of PositionToShow and NumberOfCars
        if(startIndex < 0)
          startIndex = 0;
        endIndex = rows.length - 1
      }
      rows = rows.slice(startIndex, endIndex+1)
    }
    this.tableRowModels = rows as Array<TableRowModel>
  }

  computeBestTimesIndexes(rows: Array<TableRowModel>) {
    let bestLapTime: number = Number.MAX_VALUE
    let bestLapTimeIndex: number = -1
    let bestSector1Time: number = Number.MAX_VALUE
    let bestSector1Index: number = -1
    let bestSector2Time: number = Number.MAX_VALUE
    let bestSector2Index: number = -1
    let bestSector3Time: number = Number.MAX_VALUE
    let bestSector3Index: number = -1

    for(let i = 0; i < rows.length; i++) {
      let row: TableRowModel = rows[i]

      if(row.Position === 0) continue

      if(row.BestLapTime > 0 && row.BestLapTime < bestLapTime) {
        bestLapTime = row.BestLapTime
        bestLapTimeIndex = row.PlayerIndex
      }

      if(row.BestSector1Time > 0 && row.BestSector1Time < bestSector1Time) {
        bestSector1Time = row.BestSector1Time
        bestSector1Index = row.PlayerIndex
      }

      if(row.BestSector2Time > 0 && row.BestSector2Time < bestSector2Time) {
        bestSector2Time = row.BestSector2Time
        bestSector2Index = row.PlayerIndex
      }

      if(row.BestSector3Time > 0 && row.BestSector3Time < bestSector3Time) {
        bestSector3Time = row.BestSector3Time
        bestSector3Index = row.PlayerIndex
      }
    }

    this.bestLapTimePlayerIndex = bestLapTimeIndex
    this.bestSector1PlayerIndex = bestSector1Index
    this.bestSector2PlayerIndex = bestSector2Index
    this.bestSector3PlayerIndex = bestSector3Index
  }

  private sortRowsByPosition(a: TableRowModel, b: TableRowModel): number {
    if(a.Position === 0) return 1;
    if(b.Position === 0) return -1;
    return a.Position - b.Position
  }

  getDriverNameFromIndex(index: number): string {
    let name = this.tableRowModels.find(row => row.PlayerIndex == index)?.DriverName
    return name ? name : ""
  }

  getDriverNameFromPosition(pos: number) : string {
    let name = this.tableRowModels.find(row => row.Position == pos)?.DriverName;
    return name ? name : "";
  }

  get currentColumns() {
   return this.configurationService.Settings.TableSettings.Columns.filter(col => col.enabled && this.isColumnSessionTypeAllowed(col.allowedSessionTypes, this.columnSessionType));
  }

  columnVisibleBasedOnColumnSessionType(column: number): boolean {
    return this.isColumnSessionTypeAllowed(this.configurationService.Settings.TableSettings.Columns[column].allowedSessionTypes, this.columnSessionType)
  }

  isColumnSessionTypeAllowed(a: ColumnSessionType, b: ColumnSessionType): boolean {
    if(a == ColumnSessionType.All)
      return true;

    return (a == b)
  }
}
