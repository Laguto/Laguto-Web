import { Injectable } from '@angular/core';
import {HttpClient} from "@angular/common/http";
import {Observable} from "rxjs";
import {environment} from "../../environments/environment";
import {SessionInfo} from "../models/sessionInfo";
import {AbstractService} from "./abstract.service";
import {ConnectionService} from "./connection.service";
import {SessionLength} from "../models/enums/sessionLength";
import {Track} from "../models/enums/track";
import {SessionType} from "../models/enums/sessionType";

@Injectable({
  providedIn: 'root'
})
export class SessionInfoService extends AbstractService {

  override eventId = "Session"

  sessionProgress: number = 0
  sessionInfo: SessionInfo

  constructor(private connectionService: ConnectionService) {
    super()
    this.connectionService.registerService(this);

    this.sessionInfo = {
      AirTemperature: 0,
      SessionLength: SessionLength.Full,
      SessionTime: 0,
      SessionTimeLeft: 0,
      SessionTrack: Track.Bahrain,
      SessionType: SessionType.Race,
      TrackTemperature: 0,
    }
  }

  override update(next: any) {
    let data = next as SessionInfo
    this.sessionInfo = data
    this.sessionProgress = ((data.SessionTime - data.SessionTimeLeft) / data.SessionTime) * 100
  }
}
