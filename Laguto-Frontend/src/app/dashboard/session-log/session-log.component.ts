import { Component, OnInit } from '@angular/core';
import {SessionLogService} from "../../services/session-log.service";

@Component({
  selector: 'app-session-log',
  templateUrl: './session-log.component.html',
  styleUrls: ['./session-log.component.css']
})
export class SessionLogComponent implements OnInit {
  visible: boolean;

  constructor(public sessionLogService: SessionLogService) {
    this.visible = true
  }

  ngOnInit(): void {
  }

}
