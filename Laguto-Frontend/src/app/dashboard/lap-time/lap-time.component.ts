import {Component, OnInit} from '@angular/core';
import {LapTimeService} from "../../services/lap-time.service";
import {ConfigurationService} from "../../services/configuration.service";
import {LapTimeModel} from "../../models/lapTime";

@Component({
  selector: 'app-lap-time',
  templateUrl: './lap-time.component.html',
  styleUrls: ['./lap-time.component.css']
})
export class LapTimeComponent implements OnInit {


  constructor(public lapTimeService : LapTimeService) {

  }
  ngOnInit(): void {

  }

  basicOptions = {
    plugins: {
      legend: {
        labels: {
          color: '#ebedef'
        }
      }
    },
    scales: {
      x: {
        ticks: {
          color: '#ebedef'
        },
        grid: {
          color: 'rgba(255,255,255,0.2)'
        }
      },
      y: {
        ticks: {
          color: '#ebedef'
        },
        grid: {
          color: 'rgba(255,255,255,0.2)'
        }
      }
    }
  };
}
