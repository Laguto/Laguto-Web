import {TableService} from "../../services/table.service";
import {Component, OnInit} from "@angular/core";
import {ConfigurationService} from "../../services/configuration.service";

@Component({
  selector: 'app-table',
  templateUrl: './table.component.html',
  styleUrls: ['./table.component.css']
})
export class TableComponent implements OnInit {

  constructor(public tableService: TableService, public configurationService : ConfigurationService) {}

  ngOnInit(): void {
  }
}
