import { Component, OnInit } from '@angular/core';
import {DamageService} from "../../services/damage.service";
import {ConfigurationService} from "../../services/configuration.service";
import {DamageDTO} from "../../models/DTOs/damageDTO";
import {CarDamage} from "../../models/carDamage";

@Component({
  selector: 'app-car-damage',
  templateUrl: './car-damage.component.html',
  styleUrls: ['./car-damage.component.css']
})
export class CarDamageComponent implements OnInit {

  constructor(public damageService: DamageService) {
  }

  ngOnInit(): void {
  }



}
