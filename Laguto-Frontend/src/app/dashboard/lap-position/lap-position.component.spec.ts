import { ComponentFixture, TestBed } from '@angular/core/testing';

import { LapPositionComponent } from './lap-position.component';

describe('LapPositionComponent', () => {
  let component: LapPositionComponent;
  let fixture: ComponentFixture<LapPositionComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ LapPositionComponent ]
    })
    .compileComponents();

    fixture = TestBed.createComponent(LapPositionComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
