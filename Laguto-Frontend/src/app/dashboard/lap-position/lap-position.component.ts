import {Component, OnInit} from '@angular/core';
import {LapPositionService} from "../../services/lap-position.service";
import {Color} from "chart.js";
import {FiaFlag} from "../../models/enums/fiaFlag";

@Component({
  selector: 'app-lap-position',
  templateUrl: './lap-position.component.html',
  styleUrls: ['./lap-position.component.css']
})
export class LapPositionComponent implements OnInit {

  canvas: any
  width: number = 0
  drawHeight: number = 0

  constructor(private lapPositionService: LapPositionService) {
    this.lapPositionService.lapPositionComponent = this
  }

  ngOnInit(): void {
    this.canvas = document.getElementById("lap-position");
    this.resizeCanvas()
    this.clearCanvas()

    window.onresize = (event) => this.resizeCanvas()

  }

  clearCanvas(): void {
    this.canvas.getContext('2d').clearRect(0,0,this.width, this.drawHeight*2)
  }

  resizeCanvas() {
    this.canvas.width =  window.innerWidth - 16
    this.width = this.canvas.width
    this.drawHeight = this.canvas.height / 2
  }

  drawLine() {
    let context = this.canvas.getContext('2d')
    context.beginPath()
    context.moveTo(0,this.drawHeight)
    context.lineTo(this.width,this.drawHeight)
    context.strokeStyle = 'white'
    context.stroke()
  }

  drawDriver(relativePositionOnLap: number, driverColor: Color, carPosition: number, isAi: boolean): void {
    let context = this.canvas.getContext('2d')
    context.beginPath()
    let x = relativePositionOnLap * this.width

    context.arc(x, this.drawHeight, 16, 0, 2 * Math.PI, false)

    let color = isAi ? 'rgba(116,116,116,0.8)' : `rgb(${driverColor.toString()},0.8)`
    context.fillStyle = color
    context.fill()
    context.lineWidth = '2'
    context.strokeStyle = '#010f22'
    context.stroke()

    context.font = "20px Arial"
    context.fillStyle = 'white'
    let offset = carPosition < 10 ? 6 : 12
    context.fillText(carPosition.toString(), x-offset, this.drawHeight+7)
  }

  drawPlayer(relativePositionOnLap: number, driverColor: Color, isAiControlled: boolean): void {
    let context = this.canvas.getContext('2d')
    let width = 40
    let height = 25
    let x = relativePositionOnLap * this.width + width/2

    context.beginPath()
    context.moveTo(x, this.drawHeight)
    context.lineTo(x - width, this.drawHeight - height)
    context.lineTo(x - width * 3/4, this.drawHeight)
    context.lineTo(x - width, this.drawHeight + height)
    context.closePath()

    context.strokeStyle = '#010f22'
    context.lineWidth = '2'
    context.stroke()

    let color = isAiControlled ? 'rgba(116,116,116,0.8)' : `rgb(${driverColor.toString()},0.8)`
    context.fillStyle = color
    context.fill();
  }

  drawMarshalZone(startPosition: number, endPosition: number, flag: FiaFlag): void {
    let context = this.canvas.getContext('2d')
    let x1 = startPosition * this.width
    let x2 = endPosition * this.width

    context.beginPath()
    let size = 100
    context.rect(x1, this.drawHeight-size, x2-x1, 2 * size)
    let color = this.getColorFromFlag(flag)
    context.fillStyle = color
    context.fill();
    context.strokeStyle = 'white'
    context.stroke()
  }

  getColorFromFlag(flag: FiaFlag) {
    let color
    switch(flag) {
      case FiaFlag.Red:
        color = 'rgba(255,0,0)'
        break
      case FiaFlag.Blue:
        color = 'rgba(0,50,255)'
        break
      case FiaFlag.Yellow:
        color = 'rgb(197,151,1)'
        break
      case FiaFlag.Green:
        color = 'rgba(0,100,0)'
        break
      default:
        color = `rgba(0,0,0,0)`
    }
    return color
  }

}
