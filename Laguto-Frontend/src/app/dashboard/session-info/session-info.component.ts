import { Component, OnInit } from '@angular/core';
import {SessionInfoService} from "../../services/session-info.service";

@Component({
  selector: 'app-session-info',
  templateUrl: './session-info.component.html',
  styleUrls: ['./session-info.component.css']
})
export class SessionInfoComponent implements OnInit {

  constructor(public sessionInfoService: SessionInfoService) {
  }

  ngOnInit(): void {
  }
}
