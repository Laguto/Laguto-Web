import { Component, OnInit } from '@angular/core';
import {ConfirmationService, MessageService} from "primeng/api";

import {ExportService} from "../../services/export.service";
import {environment} from "../../../environments/environment";
import {SessionResult} from "../../models/sessionResult";
import {HttpClient} from "@angular/common/http";
import {saveAs} from "file-saver";
import {formatDate} from "@angular/common";

@Component({
  selector: 'app-result',
  templateUrl: './result.component.html',
  styleUrls: ['./result.component.css'],
  providers: [MessageService]
})
export class ResultComponent implements OnInit {

  constructor(public exportService: ExportService,
              private confirmationService: ConfirmationService,
              private http: HttpClient,
              private messageService: MessageService) {
    this.exportService.resultComponent = this
  }

  ngOnInit(): void {
  }

  showPopup() {
    this.confirmationService.confirm({
      message: "Export session results as JSON file?"
    });
  }

  discard() {
    this.confirmationService.close()
  }

  saveWithoutBots() {
    this.messageService.add({severity:'success', summary:'Saved!', detail:'Result without Bots'});
    this.getSessionResults(false)
  }

  saveIncludeBots() {
    this.messageService.add({severity:'success', summary:'Saved!', detail:'Result including Bots'});
    this.getSessionResults(true)
  }

  getSessionResults(includeBots: boolean) {
    const url = environment.localToolURL + `result`
    this.http.get<SessionResult>(url).subscribe({
      next: (data) => {
        data = this.removeEmptyDriverSlots(data)
        if(!includeBots){
          data = this.removeBotsFromData(data);
        }
        this.startDownloadForResultData(data)
      },

      error: (err) => {
        console.log(err);
      }
    });
  }

  startDownloadForResultData(data: any) {
    let date = formatDate(new Date(), "dd.MM.YYYY-HH.mm", "de-DE");
    let fileName =  date + '-laguto-report.json';

    let fileToSave = new Blob([JSON.stringify(data, null, 4)], {
      type: 'application/json'
    });

    saveAs(fileToSave, fileName);
  }

  removeEmptyDriverSlots(data: SessionResult): SessionResult {
    for(let i = 0; i < data.DriverInformation.length; i++) {

      // Empty slots have position 0
      if(data.FinalClassification.FieldDriverData[i].Position == 0) {
        data.DriverInformation.splice(i, 1);
        data.FinalClassification.FieldDriverData.splice(i, 1);

        if (i < data.SessionHistories.length) {
          data.SessionHistories.splice(i, 1);
        }
        i--;
      }
    }
    data.FinalClassification.NumCars = data.FinalClassification.FieldDriverData.length
    return data;
  }

  removeBotsFromData(data: SessionResult) : SessionResult {
    for(let i = 0; i < data.DriverInformation.length; i++) {
      if (data.DriverInformation[i].IsAiControlled) {
        data.DriverInformation.splice(i, 1);
        data.FinalClassification.FieldDriverData.splice(i, 1);

        if (i < data.SessionHistories.length) {
          data.SessionHistories.splice(i, 1);
        }

        i--;
      }
    }
    data.FinalClassification.NumCars = data.FinalClassification.FieldDriverData.length
    return data;
  }
}
