import {Component, OnInit} from '@angular/core';
import {ConfigurationService} from "../services/configuration.service";
import {ConnectionService} from "../services/connection.service";
import {environment} from "../../environments/environment";
import NoSleep from 'nosleep.js';
import {TableService} from "../services/table.service";

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {

  sidebarVisible: boolean = false;
  globalContextMenuItems: any;

  binaryOptions: Array<any>

  showDirectConnectDialog: boolean = false
  toolUrl = ""

  noSleep
  wakeLockEnabled = false

  constructor(public configurationService : ConfigurationService, public connectionService: ConnectionService) {
    this.toolUrl = environment.localToolURL
    this.noSleep = new NoSleep();

    this.globalContextMenuItems = [
      {
        label: 'Open Menu',
        icon: 'pi pi-fw pi-cog',
        command: () => this.sidebarVisible = true
      },
      {
        separator:true
      },
      {
        label:'Close',
        icon:'pi pi-fw pi-times'
      },
    ];

    this.binaryOptions = [{label: 'Off', value: false}, {label: 'On', value: true}];

    // If hostname is web.laguto.de show "You need to run Laguto on your PC" dialog
    if(window.location.hostname == "web.laguto.de")
      this.showDirectConnectDialog = true

    // Set URL of the local tool
    environment.localToolURL = "http://" + window.location.hostname + ":" + environment.localToolPort + "/api/"
    this.toolUrl = "http://" + window.location.hostname + ":" + environment.localToolPort
  }

  ngOnInit(): void {
    if(window.innerWidth <= 1080) {
      this.configurationService.Settings.ViewSettings.Table = false
    }
  }

  connect() {
    this.connectionService.start()
  }

  changeHost() {
    this.connectionService.stop()
  }

  toggleWakeLock() {
    console.log(this.wakeLockEnabled)
    if(this.wakeLockEnabled)
      this.noSleep.enable()
    else
      this.noSleep.disable()
  }
}
