export enum TyreCompound {
  Soft = 16,
  Medium = 17,
  Hard = 18,
  Inter = 7,
  Wet = 8
}
