export enum SessionType {
  Unknown,
  Practice1,
  Practice2,
  Practice3,
  ShortPractice,
  Qualifying1,
  Qualifying2,
  Qualifying3,
  ShortQualifying,
  OneShotQualifying,
  Race,
  Race2,
  Race3,
  TimeTrial
}
