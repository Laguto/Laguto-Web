export enum WeatherCondition {
  Clear = 0,
  LightClouds = 1,
  Overcast = 2,
  LightRain = 3,
  HeavyRain = 4,
  Storm = 5
}
