export enum ColumnSessionType {
  Unknown,
  Race,
  NonRace,
  All
}
