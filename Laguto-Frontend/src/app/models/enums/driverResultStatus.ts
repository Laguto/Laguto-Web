export enum DriverResultStatus {
  InGarage,
  FlyingLap,
  InLap,
  OutLap,
  OnTrack,
  Finished,
  Retired,
  DNF,
  DSQ,
  InPit
}
