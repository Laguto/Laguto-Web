export enum TyreEmoji {
  "🔴" = 16, // Red
  "🟡" = 17, // Yellow
  "⚪" = 18, // White
  "🟢" = 7, // Inter
  "🔵" = 8, // Wet
}
