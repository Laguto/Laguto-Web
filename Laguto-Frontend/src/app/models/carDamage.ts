export interface CarDamage {
  FrontLeftWingDamage: number;
  FrontRightWingDamage: number;
  RearWingDamage: number;
  FloorDamage: number;
  DiffuserDamage: number;
  SidepodDamage: number;
}
