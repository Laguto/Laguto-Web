import {PenaltyType} from "../enums/penaltyType";
import {InfringementType} from "../enums/infringementType";

export interface SessionEventData {

}

export interface SessionEventDataFastestLap extends SessionEventData {
  CarIndex: number,
  LapTime: number
}

export interface SessionEventDataRetirement extends SessionEventData {
  CarIndex: number
}

export interface SessionEventDataRaceWinner extends SessionEventData {
  CarIndex: number
}

export interface SessionEventDataPenalty extends SessionEventData {
  PenaltyType: PenaltyType
  InfringementType: InfringementType
  CarIndex: number
  OtherCarIndex: number
  Time: number
  LapNumber: number
  PlacesGained: number
}

export interface SessionEventDataDriveThroughPenaltyServed extends SessionEventData {
  CarIndex: number
}

export interface SessionEventDataStopGoPenaltyServed extends SessionEventData {
  CarIndex: number
}


