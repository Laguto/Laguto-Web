import {SessionEventType} from "./eventType";
import {SessionEventData} from "./eventData";

export interface SessionEvent {
  EventType: SessionEventType
  EventDetails: SessionEventData
}
