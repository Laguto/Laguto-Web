export interface SessionLogEntry {
  title: string,
  details: string
}
