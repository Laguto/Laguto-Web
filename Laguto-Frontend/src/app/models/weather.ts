import { WeatherCondition } from "./enums/weatherCondition";
import { SessionType } from "./enums/sessionType";

export interface WeatherModel {
  OffsetTime: number;
  WeatherCondition: WeatherCondition;
  RainPercentage: number;
  TrackTemperature: number;
  AirTemperature: number;
  SessionType: SessionType;
}
