export interface DriverInformation {
  Name: string;
  IsAiControlled: boolean;
}
