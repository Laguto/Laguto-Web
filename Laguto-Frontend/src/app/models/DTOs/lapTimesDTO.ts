import {LapTimeModel} from "../lapTime";

export interface LapTimesDTO {
  LapTimes : Array<LapTimeModel>
  PlayerPosition: number
}
