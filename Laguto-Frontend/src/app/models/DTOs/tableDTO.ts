import {TableRowModel} from "../tableRow";
import {SessionType} from "../enums/sessionType";

export interface TableDTO {
  RowModels: Array<TableRowModel>;
  PlayerIndex: number;
  SessionType: SessionType
}
