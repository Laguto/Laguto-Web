import {SessionEvent} from "../sessionEvents/sessionEvent";

export interface SessionLogDTO {
  SessionLog: Array<SessionEvent>
}
