import {WeatherModel} from "../weather";

export interface WeatherDTO {
  WeatherModels: Array<WeatherModel>
}
