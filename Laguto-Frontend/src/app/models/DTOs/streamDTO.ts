export interface StreamDTO {
  event: string,
  data: any
}
