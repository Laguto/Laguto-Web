import {ColumnSessionType} from "./enums/ColumnSessionType";

export interface Settings {
  AskForResultExport: boolean;
  test: boolean;
  ViewSettings : ViewSettings;
  TableSettings: TableSettings;
}

export interface ViewSettings {
  Table: boolean;
  SessionInfo: boolean,
  TrackStatus: boolean,
  LapTime: boolean,
  CarDamage: boolean,
  Weather: boolean,
  LapPosition: boolean,
  SessionLog: boolean,
}

export interface TableSettings {
  CompactTableSettings :CompactTableSettings;
  HighlightPlayer: boolean;
  Columns: Array<{id: string, header: string, label: string, isChangeable: boolean, allowedSessionTypes: ColumnSessionType, enabled: boolean}>
}

export interface CompactTableSettings {
  Enabled: boolean,
  NumberOfCars: number
}
