import {SessionType} from "./enums/sessionType";
import {Track} from "./enums/track";
import {SessionLength} from "./enums/sessionLength";

export interface SessionInfo {
  SessionTime: number,
  SessionType: SessionType,
  SessionTrack: Track,
  SessionLength: SessionLength,
  SessionTimeLeft: number,
  AirTemperature: number,
  TrackTemperature: number
}
