import {FiaFlag} from "./enums/fiaFlag";

export interface LapPosition {
  TrackLength: number
  TrackPositions: Array<LapPositionModel>
  NumberOfMarshallZones: number
  MarshallZones: Array<MarshallZone>
  PlayerIndex: number
}

export interface LapPositionModel {
  LapDistance: number;
  Color: string;
  Position: number;
  Name: string;
  IsAiControlled: boolean
}

export interface MarshallZone {
  ZoneStart: number,
  ZoneFlag: FiaFlag
}
