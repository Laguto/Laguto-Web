import { DriverResultStatus } from "./enums/driverResultStatus";
import { TyreCompound } from "./enums/tyreCompound";

export interface TableRowModel {
  currentSector: number;
  PlayerIndex: number;
  TeamColor: string;
  Position: number;
  DriverName: string;
  IsAI: boolean;
  Status: DriverResultStatus;
  PenaltiesCombined: number;
  PositionChange: number;
  BestLapTime: number;
  DeltaToBestLapTime: number;
  DeltaToBestLapTimeOfDriverInFront: number;
  LastLapTime: number;
  DeltaLastLapToDriverInFront: number;
  GapInterval: number;
  LappedInterval: boolean,
  GapLeader: number;
  LappedLeader: number,
  TyreCompound: TyreCompound;
  TyreAge: number;
  PitCount: number;
  BestSector1Time: number;
  BestSector2Time: number;
  BestSector3Time: number;
  LastSector1Time: number;
  LastSector2Time: number;
  LastSector3Time: number;
}
