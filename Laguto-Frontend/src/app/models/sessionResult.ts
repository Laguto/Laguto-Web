import {DriverResultStatus} from "./enums/driverResultStatus";
import {DriverInformation} from "./driverInformation";

export interface SessionResult {
  FinalClassification: FinalClassification,
  SessionHistories: Array<SessionHistory>,
  DriverInformation: Array<DriverInformation>
}

export interface FinalClassification {
  NumCars: number;
  FieldDriverData: Array<FinalClassificationData>;
}

export interface FinalClassificationData {
  Position: number;
  NumberOfLaps: number;
  GridPosition: number;
  Points: number;
  NumberOfPitStops: number;
  ResultStatus: DriverResultStatus;
  BestLapTimeInMs: number;
  TotalRaceTimeInSeconds: number;
  PenaltiesTimeInSeconds: number;
  NumberOfPenalties: number;
  NumberOfTyreStints: number;
  TyreStintsActual: string;
  TyreStintsVisual: string;
  TyreStintsEndLaps: string;
}

export interface SessionHistory {
  CarIndex: number,
  NumberOfLaps: number,
  NumberOfTyreStints: number,
  BestLapTimeLapNumber: number,
  BestSector1LapNumber: number,
  BestSector2LapNumber: number,
  BestSector3LapNumber: number,
  LapHistoryData: Array<SessionHistoryLap>
  TyreStintHistoryData: Array<SessionHistoryTyreStint>
}

export interface SessionHistoryLap {
  LapTimeInMs: number,
  Sector1TimeInMs: number,
  Sector2TimeInMs: number,
  Sector3TimeInMs: number,
  LapValidBitFlags: number
}

export interface SessionHistoryTyreStint {
  EndLap: number,
  TyreStintActualCompound: number,
  TyreStintVisualCompound: number
}
