export interface LapTimeModel {
  Position: number;
  Color: string;
  LapTimes: number[];
}
