import {LOCALE_ID, NgModule} from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { FormsModule} from "@angular/forms";
import {registerLocaleData} from "@angular/common";
import localeDe from '@angular/common/locales/de';
import localeDeExtra from '@angular/common/locales/extra/de';
import {BrowserAnimationsModule} from "@angular/platform-browser/animations";

import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TableComponent } from './dashboard/table/table.component';
import { CarDamageComponent } from './dashboard/car-damage/car-damage.component';
import { SessionLogComponent } from './dashboard/session-log/session-log.component';
import { WeatherComponent } from './dashboard/weather/weather.component';
import { LapTimeComponent } from './dashboard/lap-time/lap-time.component';
import { LapPositionComponent } from './dashboard/lap-position/lap-position.component';
import { ResultComponent } from './dashboard/result/result.component';
import { SessionInfoComponent } from './dashboard/session-info/session-info.component';
import { SessionTypePipe } from './pipes/session-type.pipe';
import { WeatherIconPipe } from './pipes/weather-icon.pipe';
import { DeltaTimePipe} from "./pipes/delta-time.pipe";
import { LapTimePipe} from "./pipes/lap-time.pipe";
import  {DriverStatusPipe} from "./pipes/driver-status.pipe";
import { TyreEmojiPipe} from "./pipes/tyre-emoji.pipe";
import { SessionIsRacePipe } from './pipes/session-is-race.pipe';
import { TrackPipe } from './pipes/track.pipe';
import { TimePipe } from './pipes/time.pipe';
import { SafetyCarPipe } from './pipes/safety-car.pipe';
import { BlockableDivComponent } from './blockable-div/blockable-div.component';

import { TableModule} from "primeng/table";
import { HttpClientModule } from '@angular/common/http';
import {ImageModule} from 'primeng/image';
import { InputSwitchModule} from "primeng/inputswitch";
import {TimelineModule} from "primeng/timeline";
import {ChipModule} from "primeng/chip";
import {ButtonModule} from 'primeng/button';
import {ToastModule} from 'primeng/toast';
import {ConfirmDialogModule} from 'primeng/confirmdialog';
import {ConfirmationService, MessageService} from 'primeng/api';
import {ChartModule} from "primeng/chart";
import {Knob, KnobModule} from 'primeng/knob';
import {SplitterModule} from 'primeng/splitter';
import {TabViewModule} from 'primeng/tabview';
import {SidebarModule} from "primeng/sidebar";
import {TagModule} from "primeng/tag";
import {ScrollPanelModule} from 'primeng/scrollpanel';
import {ContextMenuModule} from 'primeng/contextmenu';
import {MenuModule} from 'primeng/menu';
import {SelectButtonModule} from 'primeng/selectbutton';
import {AccordionModule} from "primeng/accordion";
import {SliderModule} from 'primeng/slider';
import {ProgressBarModule} from 'primeng/progressbar';
import {CardModule} from 'primeng/card';
import {FieldsetModule} from 'primeng/fieldset';
import {DialogModule} from 'primeng/dialog';
import {ProgressSpinnerModule} from 'primeng/progressspinner';
import {BlockUIModule} from 'primeng/blockui';
import {Panel, PanelModule} from 'primeng/panel';
import {InputTextModule} from 'primeng/inputtext';
import {ToggleButtonModule} from 'primeng/togglebutton';
import {InputNumberModule} from 'primeng/inputnumber';
import {MultiSelectModule} from "primeng/multiselect";
import { ColumnSessionTypePipe } from './pipes/column-session-type.pipe';


registerLocaleData(localeDe, 'de-DE', localeDeExtra);
@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TableComponent,
    CarDamageComponent,
    SessionLogComponent,
    WeatherComponent,
    LapTimeComponent,
    LapPositionComponent,
    DeltaTimePipe,
    LapTimePipe,
    DriverStatusPipe,
    TyreEmojiPipe,
    WeatherIconPipe,
    SessionTypePipe,
    ResultComponent,
    SessionIsRacePipe,
    SessionInfoComponent,
    TrackPipe,
    TimePipe,
    SafetyCarPipe,
    BlockableDivComponent,
    ColumnSessionTypePipe,
  ],
    imports: [
        BrowserModule,
        BrowserAnimationsModule,
        TableModule,
        HttpClientModule,
        InputSwitchModule,
        FormsModule,
        TimelineModule,
        ImageModule,
        ChipModule,
        ToastModule,
        ButtonModule,
        ConfirmDialogModule,
        ChartModule,
        KnobModule,
        SplitterModule,
        TabViewModule,
        SidebarModule,
        TagModule,
        ScrollPanelModule,
        ContextMenuModule,
        MenuModule,
        SelectButtonModule,
        SliderModule,
        AccordionModule,
        ProgressBarModule,
        CardModule,
        FieldsetModule,
        DialogModule,
        ProgressSpinnerModule,
        BlockUIModule,
        PanelModule,
        InputTextModule,
        ToggleButtonModule,
        InputNumberModule,
        MultiSelectModule
    ],

  providers: [
    { provide: LOCALE_ID, useValue: "de-DE", },
    { provide: ConfirmationService },
    { provide: MessageService }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
